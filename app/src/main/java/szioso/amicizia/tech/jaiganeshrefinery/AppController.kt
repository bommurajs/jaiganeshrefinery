package szioso.amicizia.tech.jaiganeshrefinery


import android.graphics.Typeface
import android.support.multidex.MultiDexApplication
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatDelegate
import android.text.TextUtils

import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import szioso.amicizia.tech.jaiganeshrefinery.common.TypefaceUtil

/**
 * Created by Sankari on 9/5/2017.
 */

class AppController : MultiDexApplication() {
    private var mRequestQueue: RequestQueue? = null
    val requestQueue: RequestQueue?
        get() {
            try {
                if (mRequestQueue == null) {
                    mRequestQueue = Volley.newRequestQueue(applicationContext)
                }
            } catch (e: Exception) {
                e.toString()

            }

            return mRequestQueue
        }


    override fun onCreate() {
        super.onCreate()
        try {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
            instance = this
            //Initialize Facebook Account Kit
            TypefaceUtil.overrideFont(applicationContext, "SERIF", ResourcesCompat.getFont(this, R.font.robotoregular)!!)

            mRequestQueue = requestQueue
        } catch (e: Exception) {
            e.toString()

        }

    }

    fun <T> addToRequestQueue(req: Request<T>, tag: String) {
        try {
            req.tag = if (TextUtils.isEmpty(tag)) TAG else tag
            requestQueue!!.add(req)
        } catch (e: Exception) {
            e.toString()

        }

    }

    fun <T> addToRequestQueue(req: Request<T>) {
        try {
            req.tag = TAG
            requestQueue!!.add(req)
        } catch (e: Exception) {
            e.toString()

        }

    }

    fun cancelPendingRequests(tag: Any) {
        try {
            if (mRequestQueue != null) {
                mRequestQueue!!.cancelAll(tag)
            }
        } catch (e: Exception) {
            e.toString()

        }

    }

    companion object {


        @get:Synchronized
        var instance: AppController? = null
            private set
        val TAG = AppController::class.java!!.getSimpleName()
    }

}