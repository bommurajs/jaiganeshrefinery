package szioso.amicizia.tech.jaiganeshrefinery.common

/**
 * Created by Sankari on 9/7/2017.
 */

interface NetworkResponseListener {

    fun onSuccessResponse(url: String, response: String)
    fun onError(mURL: String, errorMessage: String)
}
