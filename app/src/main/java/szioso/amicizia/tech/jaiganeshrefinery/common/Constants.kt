package szioso.amicizia.tech.jaiganeshrefinery.common

/**
 * Created by Sankari on 9/6/2017.
 */

object Constants {


    val KEY_DEVICE_TOKEN = "devicetoken"
    val KEY_TOKEN = "Token"
    val KEY_CODE = "code"
    val KEY_SCHEMEID = "schemeId"
    val KEY_NAME = "name"


}
