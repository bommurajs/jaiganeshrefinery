package szioso.amicizia.tech.jaiganeshrefinery.views

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

import szioso.amicizia.tech.jaiganeshrefinery.R
import szioso.amicizia.tech.jaiganeshrefinery.modals.AboutResponse
import szioso.amicizia.tech.jaiganeshrefinery.modals.EnquiryResponse


/**
 * Created by Sankari on 12/21/2017.
 */

class Enquiry : Fragment(), View.OnClickListener {
    private var context: Activity? = null
    private val headerTxt: TextView? = null
    private var submitTxt: TextView? = null
    private var clearTxt: TextView? = null
    private var nameTxt: EditText? = null
    private var emailTxt: EditText? = null
    private var addressTxt: EditText? = null
    private var mobileTxt: EditText? = null
    private var commentsTxt: EditText? = null
    private var mFirebaseDatabase: DatabaseReference? = null
    private var mFirebaseInstance: FirebaseDatabase? = null
    private var submitLayer: LinearLayout? = null
    private var clearLayer: LinearLayout? = null
    private var str_name: String? = null
    private var str_email: String? = null
    private var str_mobile: String? = null
    private var str_address: String? = null
    private var str_comments: String? = null
    private val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.enquiry, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            context = activity
            /*  headerTxt=(TextView)view.findViewById(R.id.header_txt);
            headerTxt.setText(getArguments().getString(KEY_TITLE));*/
            nameTxt = view!!.findViewById<View>(R.id.input_name) as EditText
            emailTxt = view.findViewById<View>(R.id.input_email) as EditText
            addressTxt = view.findViewById<View>(R.id.input_address) as EditText
            mobileTxt = view.findViewById<View>(R.id.input_mobile) as EditText
            commentsTxt = view.findViewById<View>(R.id.input_comments) as EditText
            submitTxt = view.findViewById<View>(R.id.txt_submit) as TextView
            clearTxt = view.findViewById<View>(R.id.txt_clear) as TextView
            submitLayer = view.findViewById<View>(R.id.layout_btn_submit) as LinearLayout
            clearLayer = view.findViewById<View>(R.id.layout_btn_clear) as LinearLayout

            submitLayer!!.setOnClickListener(this)
            clearLayer!!.setOnClickListener(this)

            mFirebaseInstance = FirebaseDatabase.getInstance()
            mFirebaseDatabase = mFirebaseInstance!!.getReference("enquiryResponse")

        } catch (e: Exception) {
            e.toString()
        }

    }

    override fun onClick(view: View) {

        if (view === submitLayer) {

            if (validation()) {

                callEnquiry()
            }

        }
        if (view === clearLayer) {
            clearData()
        }

    }

    private fun callEnquiry() {
        val data = EnquiryResponse()
        data.name = nameTxt!!.text.toString()
        data.email = emailTxt!!.text.toString()
        data.address = addressTxt!!.text.toString()
        data.mobile = mobileTxt!!.text.toString()
        data.comments = commentsTxt!!.text.toString()
        val obj = EnquiryResponse.Enquiry_Response()
        obj.message = "Inserted Successfully"
        obj.status = "true"
        data.enquiryResponse = obj



        mFirebaseDatabase!!.setValue(data)

        mFirebaseDatabase!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.


                try {
                    val response = dataSnapshot.getValue<EnquiryResponse>(EnquiryResponse::class.java)


                    if (response != null) {
                        Toast.makeText(activity, response!!.enquiryResponse!!.message, Toast.LENGTH_SHORT).show()
                        clearData()

                    }

                } catch (e: Exception) {
                    e.toString()
                }


            }


            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("Hello", "Failed to read value.", error.toException())
            }
        })


    }

    private fun clearData() {
        nameTxt!!.setText("")
        emailTxt!!.setText("")
        addressTxt!!.setText("")
        mobileTxt!!.setText("")
        commentsTxt!!.setText("")
        nameTxt!!.requestFocus()
    }

    private fun validation(): Boolean {
        var status = true

        str_name = nameTxt!!.text.toString()
        str_email = emailTxt!!.text.toString()
        str_address = addressTxt!!.text.toString()
        str_mobile = mobileTxt!!.text.toString()
        str_comments = commentsTxt!!.text.toString()


        if (TextUtils.isEmpty(str_name)) {
            nameTxt!!.error = "Enter the name"
            status = false
        } else if (TextUtils.isEmpty(str_email)) {
            emailTxt!!.error = "Enter the email"
            status = false
        } else if (!TextUtils.isEmpty(str_email) && !str_email!!.matches(emailPattern.toRegex())) {
            emailTxt!!.error = "Enter the valid email"
            status = false
        } else if (TextUtils.isEmpty(str_address)) {
            addressTxt!!.error = "Enter the address"
            status = false
        } else if (TextUtils.isEmpty(str_mobile)) {
            mobileTxt!!.error = "Enter the mobile number"
            status = false
        } else if (!TextUtils.isEmpty(str_mobile) && str_mobile!!.length != 10) {
            mobileTxt!!.error = "Enter the vaid mobile number"
            status = false
        } else if (TextUtils.isEmpty(str_comments)) {
            commentsTxt!!.error = "Enter the comments"
            status = false
        }
        return status
    }

    companion object {
        private val KEY_TITLE = "key_title"

        fun newInstance(title: String): Enquiry {
            val fragment = Enquiry()
            val args = Bundle()
            args.putString(KEY_TITLE, title)
            fragment.arguments = args
            return fragment
        }
    }
}
