package szioso.amicizia.tech.jaiganeshrefinery.modals

import com.google.gson.annotations.SerializedName

/**
 * Created by Sankari on 1/2/2018.
 */

class HomeResponse {


    @SerializedName("descriptionState")
    var descriptionState: List<stateDetails>? = null

    @SerializedName("descriptionBrandWhole")
    var descriptionBrandWhole: List<Details>? = null

    @SerializedName("descriptionBrandSpilt")
    var descriptionBrandSpilt: List<Details>? = null


    @SerializedName("lastUpdatedTime")
    var lastUpdatedTime: String? = null

    @SerializedName("marketingTime")
    var marketingTime: String? = null

    class stateDetails {

        @SerializedName("description")
        var description: String? = null

        @SerializedName("buying")
        var buying: String? = null

        @SerializedName("rtgs")
        var rtgs: String? = null

        @SerializedName("retailrtgs")
        var retailrtgs: String? = null

    }

    class Details {

        @SerializedName("description")
        var description: String? = null

        @SerializedName("bid")
        var bid: String? = null

        @SerializedName("ask")
        var ask: String? = null

        @SerializedName("high")
        var high: String? = null

        @SerializedName("low")
        var low: String? = null

    }

}
