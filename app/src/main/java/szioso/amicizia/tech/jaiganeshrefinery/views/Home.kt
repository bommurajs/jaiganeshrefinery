package szioso.amicizia.tech.jaiganeshrefinery.views


import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView

import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date

import szioso.amicizia.tech.jaiganeshrefinery.R
import szioso.amicizia.tech.jaiganeshrefinery.common.CommonFunction
import szioso.amicizia.tech.jaiganeshrefinery.controller.HomeAdapter
import szioso.amicizia.tech.jaiganeshrefinery.modals.HomeResponse



class Home : Fragment() {

    private var context: Activity? = null
    private val headerTxt: TextView? = null
    private var lastUpdateDatetime: TextView? = null
    private var marketTiming: TextView? = null
    private var recylerFirstList: RecyclerView? = null
    private var recylerSecondList: RecyclerView? = null
    private var recylerThirdList: RecyclerView? = null
    private var mFirebaseDatabase: DatabaseReference? = null
    private var mFirebaseInstance: FirebaseDatabase? = null
    private var content_home: LinearLayout? = null
    private var listAdapterState: HomeAdapter? = null
    private var listAdapterWhole: HomeAdapter? = null
    private var listAdapterspilt: HomeAdapter? = null
    private var responseList: List<HomeResponse.stateDetails>? = null
    private var responseList1: List<HomeResponse.Details>? = null
    private var responseList2: List<HomeResponse.Details>? = null
    private val adapterSet = false
    private var listener: ValueEventListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.home, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            context = activity
            /* headerTxt=(TextView)view.findViewById(R.id.header_txt);
           headerTxt.setText(getArguments().getString(KEY_TITLE));*/
            //Layout Manager
            val layoutManagerFirst = LinearLayoutManager(activity)
            val layoutManagerSecond = LinearLayoutManager(activity)
            val layoutManagerThird = LinearLayoutManager(activity)

            recylerFirstList = view!!.findViewById<View>(R.id.recyclevw_state) as RecyclerView
            recylerSecondList = view.findViewById<View>(R.id.recyclevw_des_whole) as RecyclerView
            recylerThirdList = view.findViewById<View>(R.id.recyclevw_des_spilt) as RecyclerView
            recylerFirstList!!.layoutManager = layoutManagerFirst
            recylerSecondList!!.layoutManager = layoutManagerSecond
            recylerThirdList!!.layoutManager = layoutManagerThird


            lastUpdateDatetime = view.findViewById<View>(R.id.txt_last_update_datetime) as TextView
            marketTiming = view.findViewById<View>(R.id.txt_markettiming) as TextView
            content_home = view.findViewById<View>(R.id.content_home) as LinearLayout

            responseList = ArrayList()
            responseList1 = ArrayList()
            responseList2 = ArrayList()






            mFirebaseInstance = FirebaseDatabase.getInstance()
            mFirebaseDatabase = mFirebaseInstance!!.getReference("homeResponse")
            //mFirebaseInstance.setPersistenceEnabled(false);
            // mFirebaseDatabase.keepSynced(true);

            val response = HomeResponse()
            response.descriptionState = setData()
            response.descriptionBrandWhole = setData1()
            response.descriptionBrandSpilt = setData2()
            response.lastUpdatedTime = currentDateTime()
            response.marketingTime = "(Mon-Fri) : 10.00am to 9.00pm IST | (Saturday) : 9.00am to 2.00pm IST"
            mFirebaseDatabase!!.setValue(response)

            val connectedRef = mFirebaseInstance!!.getReference(".info/connected")
            connectedRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val connected = snapshot.getValue<Boolean>(Boolean::class.java)!!
                    if (connected!!) {
                        println("connected")
                        mFirebaseDatabase!!.addValueEventListener(listener)


                    } else {
                        println("not connected")
                        content_home!!.visibility = View.GONE
                    }


                }

                override fun onCancelled(error: DatabaseError) {
                    System.err.println("Listener was cancelled")
                }
            })

            CommonFunction.showProgressDialog(activity)
            listener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    try {

                        recentResponse = dataSnapshot.getValue<HomeResponse>(HomeResponse::class.java)
                        responseList = recentResponse!!.descriptionState
                        responseList1 = recentResponse!!.descriptionBrandWhole
                        responseList2 = recentResponse!!.descriptionBrandSpilt

                        if (responseList != null && responseList!!.size > 0) {

                            listAdapterState = HomeAdapter(responseList!!, activity, 1)
                            listAdapterWhole = HomeAdapter(responseList1!!, activity, 2)
                            listAdapterspilt = HomeAdapter(responseList2!!, activity, 3)
                            recylerFirstList!!.adapter = listAdapterState
                            recylerSecondList!!.adapter = listAdapterWhole
                            recylerThirdList!!.adapter = listAdapterspilt
                            listAdapterState!!.notifyDataSetChanged()
                            listAdapterWhole!!.notifyDataSetChanged()
                            listAdapterspilt!!.notifyDataSetChanged()

                        }
                        lastUpdateDatetime!!.text = " " + recentResponse!!.lastUpdatedTime!!
                        marketTiming!!.text = " " + recentResponse!!.marketingTime!!

                    } catch (e: Exception) {
                        e.toString()
                    }

                    if (recentResponse != null) {
                        previousList = HomeResponse()
                        previousList = recentResponse
                    }

                    CommonFunction.hideProgressDialog()
                    content_home!!.visibility = View.VISIBLE
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    // Failed to read value
                    Log.w("Hello", "Failed to read value.", databaseError.toException())

                    CommonFunction.hideProgressDialog()
                }

            }


        } catch (e: Exception) {
            e.toString()
        }

    }

    private fun currentDateTime(): String {
        val sdf = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
        return sdf.format(Date())
    }

    private fun setData(): List<HomeResponse.stateDetails> {
        val data = HomeResponse()
        val list = ArrayList<HomeResponse.stateDetails>()

        val item = HomeResponse.stateDetails()
        item.description = "Sliver Bar Chennai"
        item.buying = " - "
        item.rtgs = "12356"
        item.retailrtgs = " - "
        list.add(item)

        val item1 = HomeResponse.stateDetails()
        item1.description = "Sliver Bar Bangaluru"
        item1.buying = " - "
        item1.rtgs = "12346"
        item1.retailrtgs = " - "
        list.add(item1)

        val item2 = HomeResponse.stateDetails()
        item2.description = "Sliver Bar Salem"
        item2.buying = " - "
        item2.rtgs = "12336"
        item2.retailrtgs = " - "
        list.add(item2)

        val item3 = HomeResponse.stateDetails()
        item3.description = "Sliver Kacha Chennai"
        item3.buying = " - "
        item3.rtgs = "12386"
        item3.retailrtgs = " - "
        list.add(item3)

        return list


    }

    private fun setData1(): List<HomeResponse.Details> {
        val data = HomeResponse()
        val list = ArrayList<HomeResponse.Details>()

        val item = HomeResponse.Details()
        item.description = "Gold $"
        item.bid = "1283"
        item.ask = "1273"
        item.high = "15"
        item.low = "45"
        list.add(item)

        val item1 = HomeResponse.Details()
        item1.description = "Sliver $"
        item1.bid = "5454"
        item1.ask = "12121"
        item1.high = "54548"
        item1.low = "5458"
        list.add(item1)

        val item2 = HomeResponse.Details()
        item2.description = "INR"
        item2.bid = "8787"
        item2.ask = "5545"
        item2.high = "5457"
        item2.low = "15454"
        list.add(item2)

        return list
    }


    private fun setData2(): List<HomeResponse.Details> {
        val data = HomeResponse()
        val list = ArrayList<HomeResponse.Details>()

        val item = HomeResponse.Details()
        item.description = "Gold $"
        item.bid = "4544"
        item.ask = "54545"
        item.high = "65656"
        item.low = "45444"
        list.add(item)

        val item1 = HomeResponse.Details()
        item1.description = "Sliver $"
        item1.bid = "8878"
        item1.ask = "2121"
        item1.high = "6565"
        item1.low = "2122"
        list.add(item1)

        return list
    }

    override fun onStop() {

        mFirebaseDatabase!!.removeEventListener(listener!!)
        super.onStop()
    }

    companion object {
        private val KEY_TITLE = "key_title"
        private val TAG = Home::class.java!!.getSimpleName()
        var recentResponse: HomeResponse? = null
        var previousList: HomeResponse? = null

        fun newInstance(title: String): Home {
            val fragment = Home()
            val args = Bundle()
            args.putString(KEY_TITLE, title)
            fragment.arguments = args
            return fragment
        }
    }
}
