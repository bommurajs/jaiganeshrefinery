package szioso.amicizia.tech.jaiganeshrefinery.common

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.SharedPreferences
import android.graphics.Color
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.Window
import android.widget.LinearLayout

import java.lang.reflect.Field

import szioso.amicizia.tech.jaiganeshrefinery.AppController
import szioso.amicizia.tech.jaiganeshrefinery.R

/**
 * Created by Sankari on 1/9/2018.
 */

class CommonFunction {
    private fun initialize() {
        mPreference = android.preference.PreferenceManager.getDefaultSharedPreferences(AppController.instance!!.getApplicationContext())
    }

    companion object {


        private val TAG = CommonFunction::class.java!!.getSimpleName()
        private var mInstance: CommonFunction? = null
        private var mPreference: SharedPreferences? = null
        private var progressdialog: Dialog? = null

        val instance: CommonFunction
            get() {
                if (mInstance == null || mPreference == null) {
                    mInstance = CommonFunction()
                    mInstance!!.initialize()
                }
                return mInstance as CommonFunction
            }


        /**********************************Custom progressbar */
        fun showProgressDialog(activity: Activity) {
            progressdialog = Dialog(activity)
            progressdialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            progressdialog!!.setContentView(R.layout.custom_progressbar)
            progressdialog!!.window!!.setBackgroundDrawableResource(
                    R.color.transperant)
            progressdialog!!.window!!.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            progressdialog!!.setCancelable(false)
            progressdialog!!.show()


        }

        fun hideProgressDialog() {
            try {

                if (progressdialog != null && progressdialog!!.isShowing) {

                    progressdialog!!.dismiss()
                }

            } catch (e: Exception) {


            }

        }

        @SuppressLint("RestrictedApi")
        fun bottomNavigationViewRemoveShiftMode(view: BottomNavigationView) {
            val menuView = view.getChildAt(0) as BottomNavigationMenuView
            try {
                val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
                shiftingMode.setAccessible(true)
                shiftingMode.setBoolean(menuView, false)
                shiftingMode.setAccessible(false)
                for (i in 0 until menuView.childCount) {
                    val item = menuView.getChildAt(i) as BottomNavigationItemView

                    item.setShiftingMode(false)
                    // set once again checked value, so view will be updated

                    item.setChecked(item.itemData.isChecked)
                }
            } catch (e: NoSuchFieldException) {
                Log.e("BottomNav", "Unable to get shift mode field", e)
            } catch (e: IllegalAccessException) {
                Log.e("BottomNav", "Unable to change value of shift mode", e)
            }

        }

        /**********************************No Internet Alert dialog */
        fun noInternetAlertDialog(activity: Activity) {
            val alertDialog = AlertDialog.Builder(activity)

            alertDialog.setTitle("No Internet Connection Available !")

            alertDialog.setPositiveButton("Retry") { dialog, which ->
                //dialog.cancel();
                activity.startActivity(activity.intent)
            }

            alertDialog.show()
        }
    }

}
