package szioso.amicizia.tech.jaiganeshrefinery.modals

import com.google.gson.annotations.SerializedName

/**
 * Created by Sankari on 1/2/2018.
 */

class AboutResponse {


    @SerializedName("aboutUs")
    var aboutUs: AboutUs? = null

    class AboutUs {

        @SerializedName("objective")
        var objective: String? = null

        @SerializedName("products")
        var products: List<String>? = null

        @SerializedName("productDescription")
        var productDescription: String? = null

        @SerializedName("vision")
        var vision: String? = null

        @SerializedName("mission")
        var mission: String? = null

    }
}

