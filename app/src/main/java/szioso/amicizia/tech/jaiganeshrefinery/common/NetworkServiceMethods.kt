package szioso.amicizia.tech.jaiganeshrefinery.common

import android.app.Activity
import android.os.Handler

import java.util.HashMap

/**
 * Created by Sankari on 9/7/2017.
 */

class NetworkServiceMethods {
    private val handler = Handler()

    fun homeCall(requestParams: HashMap<String, String>, servicetype: Int, activity: Activity, networkListener: NetworkResponseListener) {
        NetworkServiceHandler.instance.httpPOST(APIMethods.GET_HOME, requestParams, servicetype, activity, networkListener)
    }

    fun aboutCall(requestParams: HashMap<String, String>, servicetype: Int, activity: Activity, networkListener: NetworkResponseListener) {
        NetworkServiceHandler.instance.httpPOST(APIMethods.GET_ABOUTUS, requestParams, servicetype, activity, networkListener)
    }

    fun contactCall(requestParams: HashMap<String, String>, servicetype: Int, activity: Activity, networkListener: NetworkResponseListener) {
        NetworkServiceHandler.instance.httpPOST(APIMethods.GET_CONTACTUS, requestParams, servicetype, activity, networkListener)
    }

    fun enquiryCall(requestParams: HashMap<String, String>, servicetype: Int, activity: Activity, networkListener: NetworkResponseListener) {
        NetworkServiceHandler.instance.httpPOST(APIMethods.GET_ENQUIRY, requestParams, servicetype, activity, networkListener)
    }

    companion object {


        private val TAG = NetworkServiceMethods::class.java!!.getSimpleName()
        private var mInstance: NetworkServiceMethods? = null

        val instance: NetworkServiceMethods
            get() {
                if (mInstance == null) {
                    mInstance = NetworkServiceMethods()

                }
                return mInstance as NetworkServiceMethods
            }
    }


}
