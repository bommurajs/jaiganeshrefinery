package szioso.amicizia.tech.jaiganeshrefinery.views

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager

import szioso.amicizia.tech.jaiganeshrefinery.R


/**
 * Created by Sankari on 12/22/2017.
 */

class Splash : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {

            setContentView(R.layout.splash_screen)
            //Hide Action Bar and Status Bar
            val attrs = window.attributes
            attrs.flags = attrs.flags or WindowManager.LayoutParams.FLAG_FULLSCREEN
            window.attributes = attrs

            val SPLASH_TIME_OUT = 3000
            Handler().postDelayed(/*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            {
                val i = Intent(this@Splash, BottomNavigationMainActivity::class.java)
                startActivity(i)
                finish()
            }, SPLASH_TIME_OUT.toLong())
        } catch (e: Exception) {
            e.toString()
        }

    }
}
