package szioso.amicizia.tech.jaiganeshrefinery.common

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.android.volley.*
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject
import szioso.amicizia.tech.jaiganeshrefinery.AppController
import java.io.UnsupportedEncodingException
import java.util.*


class NetworkServiceHandler {
    private val ServiceType = ""
    internal lateinit var con: Context
    internal var commonbundledata: Any? = null
    private var JSONObjectData: JSONObject? = null


    fun checkInternetConnection(context: Activity): Boolean {
        var connection = false

        try {
            con = context
            val check = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            if (check != null) {
                val info = check.allNetworkInfo

                if (info != null)
                    for (i in info.indices)
                        if (info[i].state == NetworkInfo.State.CONNECTED) {
                            connection = true
                        }
            }
        } catch (e: Exception) {
            e.toString()
        }

        if (!connection && !InternalStorage.instance.badconnection) {
            CommonFunction.noInternetAlertDialog(context)

        }

        return connection
    }

    fun httpPOST(url: String, requestParams: HashMap<String, String>, servicetype: Int, activity: Activity, networkListener: NetworkResponseListener) {
        commonbundledata = null
        httpPostExecution(url, requestParams, servicetype, activity, networkListener)

    }

    fun httpPOST(url: String, requestObject: Any, servicetype: Int, activity: Activity, networkListener: NetworkResponseListener) {
        commonbundledata = requestObject
        httpPostExecution(url, null, servicetype, activity, networkListener)

    }


    fun httpPostExecution(url: String, requestParams: HashMap<String, String>?, servicetype: Int, activity: Activity, networkListener: NetworkResponseListener) {

        try {


            var mRequestParams = HashMap<String, String>()
            val _activity = activity

            val queue = AppController.instance!!.requestQueue

            if (commonbundledata != null) {
                val gsonInstance = Gson()
                JSONObjectData = JSONObject(gsonInstance.toJson(commonbundledata))
            } else if (requestParams != null) {
                mRequestParams = requestParams
                JSONObjectData = JSONObject(mRequestParams)
            }

            //BaseActivity.showProgressDialog(_activity);


            val jsonObjectRequest = object : JsonObjectRequest(servicetype, url, JSONObjectData, Response.Listener { response ->
                //BaseActivity.hideProgressDialog();
                networkListener.onSuccessResponse(url, response.toString())
            }, Response.ErrorListener { error ->
                // BaseActivity.hideProgressDialog();
                var message: String? = null
                if (error is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (error is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (error is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (error is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (error is NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (error is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                }
                networkListener.onError(url, message!!)
            }) {

                override fun parseNetworkResponse(response: NetworkResponse): Response<JSONObject> {
                    try {
                        val jsonString = String(response.data,HttpHeaderParser.parseCharset(response.headers))
                        return Response.success(JSONObject(jsonString),
                                HttpHeaderParser.parseCacheHeaders(response))
                    } catch (e: UnsupportedEncodingException) {
                        return Response.error(ParseError(e))
                    } catch (je: JSONException) {
                        return Response.error(ParseError(je))
                    }

                }

                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val headers = HashMap<String, String>()
                    if (InternalStorage.instance.token != null && InternalStorage.instance.token.length > 0) {

                        headers["Authorization"] = "Bearer " + InternalStorage.instance.token
                    }
                    return headers
                }
            }
            val socketTimeout = 30000//30 seconds - change to what you want
            val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            jsonObjectRequest.retryPolicy = policy
            AppController.instance!!.addToRequestQueue(jsonObjectRequest)
        } catch (e: Exception) {
            e.toString()
        }

    }

    private fun String(bytes: ByteArray?, charset: String?): String {
        return String(bytes,charset)
    }

    companion object {

        private val TAG = NetworkServiceHandler::class.java!!.getSimpleName()
        private var mInstance: NetworkServiceHandler? = null

        val instance: NetworkServiceHandler
            get() {
                if (mInstance == null) {
                    mInstance = NetworkServiceHandler()
                }
                return mInstance as NetworkServiceHandler
            }
    }


}
