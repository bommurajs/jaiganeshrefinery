package szioso.amicizia.tech.jaiganeshrefinery.common


import szioso.amicizia.tech.jaiganeshrefinery.BuildConfig

/**
 * Created by Sankari on 9/5/2017.
 */

object APIMethods {

    //device
    var BASE_SERVICE_URL = BuildConfig.BASE_SERVICE_URL
    val GET_HOME = BASE_SERVICE_URL + "account-kit/login"
    val GET_ABOUTUS = BASE_SERVICE_URL + "auth/user/profileDetails"
    val GET_CONTACTUS = BASE_SERVICE_URL + "auth/user/updateProfile"
    val GET_ENQUIRY = BASE_SERVICE_URL + "schemes"


}
