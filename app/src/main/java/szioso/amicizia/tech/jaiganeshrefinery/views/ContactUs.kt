package szioso.amicizia.tech.jaiganeshrefinery.views

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

import java.util.ArrayList

import szioso.amicizia.tech.jaiganeshrefinery.R
import szioso.amicizia.tech.jaiganeshrefinery.modals.ContactResponse


/**
 * Created by Sankari on 12/21/2017.
 */

class ContactUs : Fragment(), OnMapReadyCallback {
    private var context: Activity? = null
    private val headerTxt: TextView? = null
    private var reachusTxt: TextView? = null
    private var callusTxt: TextView? = null
    private var centerxTxt: TextView? = null
    private var emailTXt: TextView? = null
    private var mMap: GoogleMap? = null
    private var mMapView: MapView? = null
    private var companyName: String? = null
    private var address: String? = null
    private var latitude: String? = null
    private var longitude: String? = null
    private var mFirebaseDatabase: DatabaseReference? = null
    private var mFirebaseInstance: FirebaseDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view: View? = null
        try {
            view = inflater!!.inflate(R.layout.contactus, container, false)
            mMapView = view!!.findViewById<View>(R.id.mapView) as MapView
            mMapView!!.onCreate(savedInstanceState)
            mMapView!!.onResume()

            try {
                MapsInitializer.initialize(activity.applicationContext)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            mMapView!!.getMapAsync(this)

        } catch (e: Exception) {
            e.toString()
        }

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            context = activity
            /*  headerTxt=(TextView)view.findViewById(R.id.header_txt);
            headerTxt.setText(getArguments().getString(KEY_TITLE));*/


            reachusTxt = view!!.findViewById<View>(R.id.txt_reachus) as TextView
            callusTxt = view.findViewById<View>(R.id.txt_callus) as TextView
            centerxTxt = view.findViewById<View>(R.id.txt_centerx) as TextView
            emailTXt = view.findViewById<View>(R.id.txt_email) as TextView

            mFirebaseInstance = FirebaseDatabase.getInstance()
            mFirebaseDatabase = mFirebaseInstance!!.getReference("contactUs")

            var response = ContactResponse()
            response = setData()
            mFirebaseDatabase!!.setValue(response)

            mFirebaseDatabase!!.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.


                    try {
                        val response = dataSnapshot.getValue<ContactResponse>(ContactResponse::class.java)


                        if (response != null) {
                            setMap(response)
                            setValue(response)

                        }

                    } catch (e: Exception) {
                        e.toString()
                    }


                }

                override fun onCancelled(error: DatabaseError) {
                    // Failed to read value
                    Log.w("Hello", "Failed to read value.", error.toException())
                }
            })


        } catch (e: Exception) {
            e.toString()
        }


    }

    private fun setValue(response: ContactResponse?) {
        reachusTxt!!.text = response!!.contactUs!!.address

        val strBuilder = StringBuilder()
        for (i in response.contactUs!!.phoneNo!!.indices) {

            strBuilder.append(response.contactUs!!.phoneNo!![i])
            strBuilder.append("\n")

        }
        callusTxt!!.text = strBuilder.toString()

        val strBuilder1 = StringBuilder()
        for (i in response.contactUs!!.centerx!!.indices) {
            strBuilder1.append(response.contactUs!!.centerx!![i])
            strBuilder1.append("\n")

        }
        centerxTxt!!.text = strBuilder1.toString()
        emailTXt!!.text = response.contactUs!!.email
    }

    private fun setData(): ContactResponse {

        val data = ContactResponse()
        val obj = ContactResponse.ContactAddress()
        obj.companyName = "JGR"
        val strBuilder = StringBuilder()
        strBuilder.append("# 49, N.S.C Bose Road,")
        strBuilder.append("\n")
        strBuilder.append("Shop No:1,1st Floor,")
        strBuilder.append("\n")
        strBuilder.append("Sowcarpet,Chennai,")
        strBuilder.append("\n")
        strBuilder.append("Tamilnadu.")

        obj.address = strBuilder.toString()
        obj.latitude = "13.089566"
        obj.longitude = "80.278682"
        obj.phoneNo = ArrayList()
        (obj.phoneNo as ArrayList<String>).add("+91 44 4500 0703")
        (obj.phoneNo as ArrayList<String>).add("+91 44 3257 1127")
        obj.centerx = ArrayList()
        (obj.centerx as ArrayList<String>).add("+91 44 4500 0703")
        (obj.centerx as ArrayList<String>).add("+91 44 3257 1127")
        obj.email = "bbs@gmail.com"
        data.contactUs = obj


        return data
    }


    override fun onMapReady(googleMap: GoogleMap) {
        try {
            mMap = googleMap
            setMap(null)
        } catch (e: Exception) {
            e.toString()

        }

    }

    private fun setMap(response: ContactResponse?) {
        try {
            if (response != null) {
                latitude = response.contactUs!!.latitude
                longitude = response.contactUs!!.longitude
                companyName = response.contactUs!!.companyName
                address = response.contactUs!!.address

                val location = LatLng(java.lang.Double.parseDouble(latitude), java.lang.Double.parseDouble(longitude))
                mMap!!.addMarker(MarkerOptions().position(location)
                        .title(companyName).snippet(address))

                mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 13f))

                mMap!!.uiSettings.isZoomGesturesEnabled = false
            }


        } catch (e: Exception) {

        }


    }


    override fun onResume() {
        super.onResume()
        mMapView!!.onResume()
    }

    companion object {
        private val KEY_TITLE = "key_title"


        fun newInstance(title: String): ContactUs {
            val fragment = ContactUs()
            val args = Bundle()
            args.putString(KEY_TITLE, title)
            fragment.arguments = args
            return fragment
        }
    }


}
