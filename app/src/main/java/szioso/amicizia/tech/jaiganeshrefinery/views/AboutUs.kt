package szioso.amicizia.tech.jaiganeshrefinery.views


import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.TextView

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

import java.util.ArrayList

import szioso.amicizia.tech.jaiganeshrefinery.R
import szioso.amicizia.tech.jaiganeshrefinery.modals.AboutResponse


/**
 * Created by Sankari on 12/21/2017.
 */

class AboutUs : Fragment() {
    private var context: Activity? = null
    private val headerTxt: TextView? = null
    private var objectiveTxt: TextView? = null
    private var productDescriptionTxt: TextView? = null
    private var missionTxt: TextView? = null
    private var visionTxt: TextView? = null
    private var mFirebaseDatabase: DatabaseReference? = null
    private var mFirebaseInstance: FirebaseDatabase? = null
    private var linearProduct: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.aboutus, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            context = activity
            /*  headerTxt=(TextView)view.findViewById(R.id.header_txt);
            headerTxt.setText(getArguments().getString(KEY_TITLE));*/

            objectiveTxt = view!!.findViewById<View>(R.id.txt_objective) as TextView
            productDescriptionTxt = view.findViewById<View>(R.id.txt_product_description) as TextView
            missionTxt = view.findViewById<View>(R.id.txt_mission_value) as TextView
            visionTxt = view.findViewById<View>(R.id.txt_vision_value) as TextView
            linearProduct = view.findViewById<View>(R.id.layout_product_list) as LinearLayout


            mFirebaseInstance = FirebaseDatabase.getInstance()
            mFirebaseDatabase = mFirebaseInstance!!.getReference("aboutUs")

            var response = AboutResponse()
            response = setData()
            mFirebaseDatabase!!.setValue(response)


            mFirebaseDatabase!!.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.


                    try {
                        val response = dataSnapshot.getValue<AboutResponse>(AboutResponse::class.java)


                        if (response != null) {
                            setValue(response)

                        }

                    } catch (e: Exception) {
                        e.toString()
                    }


                }


                override fun onCancelled(error: DatabaseError) {
                    // Failed to read value
                    Log.w("Hello", "Failed to read value.", error.toException())
                }
            })


        } catch (e: Exception) {
            e.toString()
        }


    }

    private fun setValue(response: AboutResponse?) {

        try {
            if (response != null) {
                objectiveTxt!!.text = response.aboutUs!!.objective
                productDescriptionTxt!!.text = response.aboutUs!!.productDescription
                visionTxt!!.text = response.aboutUs!!.vision
                missionTxt!!.text = response.aboutUs!!.mission

                val lparams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                for (i in response.aboutUs!!.products!!.indices) {
                    val tv = TextView(activity)
                    tv.layoutParams = lparams
                    tv.text = response.aboutUs!!.products!![i]
                    tv.textSize = 12f
                    tv.setPadding(Math.round(context!!.resources.getDimension(R.dimen._5sdp)),
                            Math.round(context!!.resources.getDimension(R.dimen._5sdp)),
                            Math.round(context!!.resources.getDimension(R.dimen._5sdp)),
                            0)
                    tv.setTextColor(activity.resources.getColor(R.color.black))
                    tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.coin, 0, 0, 0)
                    tv.compoundDrawablePadding = Math.round(context!!.resources.getDimension(R.dimen._5sdp))
                    linearProduct!!.addView(tv)
                }

            }
        } catch (e: Exception) {
            e.toString()
        }

    }

    private fun setData(): AboutResponse {
        val data = AboutResponse()

        val obj = AboutResponse.AboutUs()

        obj.objective = "Sri Hari Om Jewelers is among one of certified silver bullion dealers in the country located in Chennai, specializing in bars and coins of precious metal like Silver. Sri Hari Om Jewelers laid its first step into the bullion market in the year 2002. With its unique order matching platform it facilitates efficient consumer and small business transactions Sri Hari Om Jewelers specializes in the buying and selling of silver, particularly bullion and it offers a full catalogue of silver products."
        obj.mission = "Profitable growth through superior customer service, quality and commitment"
        obj.vision = "To be a leader in precious metal trading in India by making bullion available at competitive prices through customer satisfaction in every part of the country.\n" + "\n" + "Sri Hari Om Jewelershad dedicated themselves to continuous improvements which enable them to meet the exacting requirements of their customers. This bond of trust has helped Sri Hari Om Jewelers attain significant feats in bullion market."
        val productList = ArrayList<String>()
        productList.add("Gold")
        productList.add("Sliver")
        obj.products = ArrayList()
        obj.products = productList
        obj.productDescription = "Their combined knowledge and experience has been instrumental in making Sri Hari Om Jewellers as a company where the clients return time and time again."

        data.aboutUs = obj
        return data

    }

    companion object {
        private val KEY_TITLE = "key_title"

        fun newInstance(title: String): AboutUs {
            val fragment = AboutUs()
            val args = Bundle()
            args.putString(KEY_TITLE, title)
            fragment.arguments = args
            return fragment
        }
    }
}
