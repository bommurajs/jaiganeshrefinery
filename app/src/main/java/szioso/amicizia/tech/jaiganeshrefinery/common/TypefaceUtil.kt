package szioso.amicizia.tech.jaiganeshrefinery.common

import android.content.Context
import android.graphics.Typeface
import android.os.Build

import java.lang.reflect.Field
import java.sql.DriverManager.println
import java.util.HashMap

/**
 * Created by Sankari on 10/10/2017.
 */

object TypefaceUtil {

    fun overrideFont(context: Context, defaultFontNameToOverride: String, customFontFileName: Typeface) {


        //  final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileName);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val newMap = HashMap<String, Typeface>()
            newMap["serif"] = customFontFileName
            try {
                val staticField = Typeface::class.java!!
                        .getDeclaredField("sSystemFontMap")
                staticField.setAccessible(true)
                staticField.set(null, newMap)
            } catch (e: NoSuchFieldException) {
                e.printStackTrace()
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            }

        } else {
            try {
                val defaultFontTypefaceField = Typeface::class.java!!.getDeclaredField(defaultFontNameToOverride)
                defaultFontTypefaceField.setAccessible(true)
                defaultFontTypefaceField.set(null, customFontFileName)
            } catch (e: Exception) {
                println("Can not set custom roboto $customFontFileName instead of $defaultFontNameToOverride")
            }

        }
    }
}
