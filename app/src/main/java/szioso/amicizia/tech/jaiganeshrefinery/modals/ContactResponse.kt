package szioso.amicizia.tech.jaiganeshrefinery.modals

import com.google.gson.annotations.SerializedName

/**
 * Created by Sankari on 1/2/2018.
 */

class ContactResponse {


    @SerializedName("contactUs")
    var contactUs: ContactAddress? = null

    class ContactAddress {

        @SerializedName("companyName")
        var companyName: String? = null

        @SerializedName("email")
        var email: String? = null

        @SerializedName("phoneNo")
        var phoneNo: List<String>? = null

        @SerializedName("centerx")
        var centerx: List<String>? = null


        @SerializedName("address")
        var address: String? = null


        @SerializedName("latitude")
        var latitude: String? = null

        @SerializedName("longitude")
        var longitude: String? = null


    }
}
