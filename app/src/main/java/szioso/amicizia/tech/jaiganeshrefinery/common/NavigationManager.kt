package szioso.amicizia.tech.jaiganeshrefinery.common

/**
 * @author msahakyan
 */

interface NavigationManager {


    fun showFragmentHome(home: String)

    fun showFragmentAbout(title: String)

    fun showFragmentContact(title: String)

    fun showFragmentEnquiry(title: String)

}
