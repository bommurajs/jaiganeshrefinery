package szioso.amicizia.tech.jaiganeshrefinery.common

import android.annotation.SuppressLint
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction

import szioso.amicizia.tech.jaiganeshrefinery.BuildConfig
import szioso.amicizia.tech.jaiganeshrefinery.R
import szioso.amicizia.tech.jaiganeshrefinery.views.AboutUs
import szioso.amicizia.tech.jaiganeshrefinery.views.BottomNavigationMainActivity
import szioso.amicizia.tech.jaiganeshrefinery.views.ContactUs
import szioso.amicizia.tech.jaiganeshrefinery.views.Enquiry
import szioso.amicizia.tech.jaiganeshrefinery.views.Home

/**
 * @author msahakyan
 */

class FragmentNavigationManager : NavigationManager {

    private var mFragmentManager: FragmentManager? = null
    private var mActivity: BottomNavigationMainActivity? = null
    private var count: Int = 0

    private fun configure(activity: BottomNavigationMainActivity) {
        try {
            mActivity = activity
            mFragmentManager = mActivity!!.supportFragmentManager
            count = mFragmentManager!!.backStackEntryCount
        } catch (e: Exception) {
            e.toString()

        }

    }

    override fun showFragmentHome(title: String) {
        showFragment(Home.newInstance(title), title, false)
    }

    override fun showFragmentAbout(title: String) {
        showFragment(AboutUs.newInstance(title), title, false)
    }

    override fun showFragmentContact(title: String) {
        showFragment(ContactUs.newInstance(title), title, false)
    }

    override fun showFragmentEnquiry(title: String) {
        showFragment(Enquiry.newInstance(title), title, false)
    }


    private fun showFragment(fragment: Fragment, title: String, allowStateLoss: Boolean) {
        try {
            val fm = mFragmentManager
            // fm.addOnBackStackChangedListener(this);
            @SuppressLint("CommitTransaction")
            val ft = fm!!.beginTransaction()
                    .replace(R.id.content_frame, fragment, title)

            ft.addToBackStack(null)

            if (allowStateLoss || !BuildConfig.DEBUG) {
                ft.commitAllowingStateLoss()
            } else {

            }
            ft.commit()
            fm.executePendingTransactions()
        } catch (e: Exception) {
            e.toString()

        }

    }

    companion object {

        private var sInstance: FragmentNavigationManager? = null

        fun obtain(activity: BottomNavigationMainActivity): FragmentNavigationManager? {
            try {
                if (sInstance == null) {
                    sInstance = FragmentNavigationManager()
                }
                sInstance!!.configure(activity)
            } catch (e: Exception) {
                e.toString()

            }

            return sInstance
        }
    }


}
