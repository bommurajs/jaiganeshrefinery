package szioso.amicizia.tech.jaiganeshrefinery.common

import android.content.SharedPreferences

import szioso.amicizia.tech.jaiganeshrefinery.AppController

/**
 * Created by Sankari on 9/6/2017.
 */

class InternalStorage {
    var badconnection: Boolean = false

    // set Device Token
    var deviceToken: String
        get() = mPreference!!.getString(Constants.KEY_DEVICE_TOKEN, "")
        set(token) {
            val editor = mPreference!!.edit()
            editor.putString(Constants.KEY_DEVICE_TOKEN, token)
            editor.apply()
            editor.commit()
        }

    // set Token
    var token: String
        get() = mPreference!!.getString(Constants.KEY_TOKEN, "")
        set(token) {
            val editor = mPreference!!.edit()
            editor.putString(Constants.KEY_TOKEN, token)
            editor.apply()
            editor.commit()
        }

    private fun initialize() {
        mPreference = android.preference.PreferenceManager.getDefaultSharedPreferences(AppController.instance!!.getApplicationContext())
    }

    companion object {


        private val TAG = InternalStorage::class.java!!.getSimpleName()
        private var mInstance: InternalStorage? = null
        private var mPreference: SharedPreferences? = null


        val instance: InternalStorage
            get() {
                if (mInstance == null || mPreference == null) {
                    mInstance = InternalStorage()
                    mInstance!!.initialize()
                }
                return mInstance as InternalStorage
            }
    }


}
