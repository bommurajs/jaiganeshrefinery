package szioso.amicizia.tech.jaiganeshrefinery.controller

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView

import java.util.HashMap

import szioso.amicizia.tech.jaiganeshrefinery.R
import szioso.amicizia.tech.jaiganeshrefinery.modals.HomeResponse
import szioso.amicizia.tech.jaiganeshrefinery.modals.HomeResponse.stateDetails
import szioso.amicizia.tech.jaiganeshrefinery.views.Home

import android.animation.ObjectAnimator.*

/**
 * Created by Sankari on 1/2/2018.
 */

class HomeAdapter : RecyclerView.Adapter<HomeAdapter.ViewHolder> {
    private var currentList: List<HomeResponse.Details>?=null
    private var currentStateList: List<HomeResponse.stateDetails>?=null
    private var previousList: HomeResponse? = null
    private var context: Context? = null
    private var identifyRow: Int = 0
    private val firsttime = false


    constructor(responseList: List<stateDetails>, activity: Context, _identifyRow: Int) {
        this.currentStateList = responseList
        this.context = activity
        this.identifyRow = _identifyRow
        this.previousList = Home.previousList
    }

    constructor(responseList1: List<HomeResponse.Details>, activity: Activity, _identifyRow: Int) {
        this.currentList = responseList1
        this.context = activity
        this.identifyRow = _identifyRow
        this.previousList = Home.previousList
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeAdapter.ViewHolder {
        var view: View? = null
        if (identifyRow == 1) {
            view = LayoutInflater.from(context).inflate(R.layout.home_list_row_state, parent, false)
        } else {
            //identify list 2 &  3 common
            view = LayoutInflater.from(context).inflate(R.layout.home_list_row, parent, false)
        }
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: HomeAdapter.ViewHolder, position: Int) {


        if (identifyRow == 1) {
            val item = currentStateList!![position]

            if (previousList != null) {
                chaningColor(item.rtgs, previousList!!.descriptionState!![position].rtgs, holder.txt_rtgs_state)

            }

            holder.txt_description_state!!.text = item.description
            holder.txt_buying_state!!.text = item.buying
            holder.txt_retailsrtgs_state!!.text = item.retailrtgs
            holder.txt_rtgs_state!!.text = item.rtgs


        } else {
            //identify list 2 &  3 common
            dataSetting(this!!.currentList!!, position, holder)
        }


    }

    private fun chaningColor(currentListRtgs: String?, prevListRtgs: String?, txtView: TextView?) {

        if (currentListRtgs != null && prevListRtgs != null) {
            if (Integer.parseInt(currentListRtgs) > Integer.parseInt(prevListRtgs)) {
                animation(txtView, "backgroundColor", context!!.resources.getColor(R.color.green))

            } else if (Integer.parseInt(currentListRtgs) < Integer.parseInt(prevListRtgs)) {
                animation(txtView, "backgroundColor", context!!.resources.getColor(R.color.red))
            }


        }

    }


    private fun dataSetting(currentList1: List<HomeResponse.Details>, position: Int, holder: ViewHolder) {

        val item = currentList1[position]

        if (identifyRow == 2) {
            if (previousList != null) {
                chaningColor(item.bid, previousList!!.descriptionBrandWhole!![position].bid, holder.txt_bid)
                chaningColor(item.ask, previousList!!.descriptionBrandWhole!![position].ask, holder.txt_ask)
            }

        } else {
            //identify list3
            if (previousList != null) {
                chaningColor(item.bid, previousList!!.descriptionBrandSpilt!![position].bid, holder.txt_bid)
                chaningColor(item.ask, previousList!!.descriptionBrandSpilt!![position].ask, holder.txt_ask)
            }
        }

        holder.txt_description!!.text = item.description
        holder.txt_high!!.text = item.high
        holder.txt_low!!.text = item.low
        holder.txt_bid!!.text = item.bid
        holder.txt_ask!!.text = item.ask

    }

    override fun getItemCount(): Int {
        return if (identifyRow == 1) {
            currentStateList!!.size
        } else {
            //identify list 2 &  3 common
            currentList!!.size
        }


    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var txt_description: TextView? = null
        var txt_bid: TextView? = null
        var txt_ask: TextView? = null
        var txt_high: TextView? = null
        var txt_low: TextView? = null
        var txt_description_state: TextView? = null
        var txt_buying_state: TextView? = null
        var txt_retailsrtgs_state: TextView? = null
        var txt_rtgs_state: TextView? = null

        init {
            if (identifyRow == 1) {
                txt_description_state = itemView.findViewById<View>(R.id.txt_description_state) as TextView
                txt_buying_state = itemView.findViewById<View>(R.id.txt_buying_state) as TextView
                txt_rtgs_state = itemView.findViewById<View>(R.id.txt_rtgs_state) as TextView
                txt_retailsrtgs_state = itemView.findViewById<View>(R.id.txt_retailrtgs_state) as TextView

            } else {
                //identify list 2 &  3 common
                txt_description = itemView.findViewById<View>(R.id.txt_description) as TextView
                txt_bid = itemView.findViewById<View>(R.id.txt_bid) as TextView
                txt_ask = itemView.findViewById<View>(R.id.txt_ask) as TextView
                txt_high = itemView.findViewById<View>(R.id.txt_high) as TextView
                txt_low = itemView.findViewById<View>(R.id.txt_low) as TextView
            }

        }
    }


    private fun animation(txtview: TextView?, propertyName: String, txtColor: Int) {

        if (txtview != null) {

            txtview.setTextColor(context!!.resources.getColor(R.color.white))
            val anim = ObjectAnimator.ofInt(txtview, propertyName, txtColor, Color.WHITE,
                    txtColor)
            anim.duration = 1200
            anim.setEvaluator(ArgbEvaluator())
            anim.repeatCount = 5
            anim.start()
            anim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    txtview.setBackgroundColor(context!!.resources.getColor(R.color.gainsboro))
                }
            })


        }

    }

    internal companion object {
        var oldText: HashMap<String, HashMap<String, List<String>>>? = null
    }
}
