package szioso.amicizia.tech.jaiganeshrefinery.modals

import com.google.gson.annotations.SerializedName

/**
 * Created by Sankari on 1/2/2018.
 */

class EnquiryResponse {


    @SerializedName("enquiryResponse")
    var enquiryResponse: Enquiry_Response? = null


    @SerializedName("name")
    var name: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("mobile")
    var mobile: String? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("comments")
    var comments: String? = null

    class Enquiry_Response {

        @SerializedName("message")
        var message: String? = null

        @SerializedName("status")
        var status: String? = null

    }

}
