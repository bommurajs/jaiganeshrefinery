package szioso.amicizia.tech.jaiganeshrefinery.views

import android.app.Activity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import szioso.amicizia.tech.jaiganeshrefinery.R
import szioso.amicizia.tech.jaiganeshrefinery.common.CommonFunction
import szioso.amicizia.tech.jaiganeshrefinery.common.FragmentNavigationManager
import szioso.amicizia.tech.jaiganeshrefinery.common.NavigationManager

class BottomNavigationMainActivity : AppCompatActivity() {


    internal var bottomNavigation: BottomNavigationView? = null
    private var mNavigationManager: NavigationManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bottom_navigation_main)

        mNavigationManager = FragmentNavigationManager.obtain(this)

        val bottomNavigationView = findViewById<View>(R.id.bottom_navigation_view) as BottomNavigationView
        CommonFunction.bottomNavigationViewRemoveShiftMode(bottomNavigationView)

        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_home -> mNavigationManager!!.showFragmentHome("Home")
                R.id.action_aboutus -> mNavigationManager!!.showFragmentAbout("About Us")
                R.id.action_contactus -> mNavigationManager!!.showFragmentContact("Contact Us")
                R.id.action_enquiry -> mNavigationManager!!.showFragmentEnquiry("Enquiry")
            }


            true
        }


        mNavigationManager!!.showFragmentHome("Home")
    }

    override fun onBackPressed() {
        finish()
    }

    fun hideSoftKeyBoard(view: View) {
        try {
            val inputMethodManager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            if (this.currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(this.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

            }
        } catch (e: Exception) {

        }

    }


}
